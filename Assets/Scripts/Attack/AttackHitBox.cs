using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class AttackHitBox : MonoBehaviour, IDamageable
{
    public abstract void ApplyDamage(GameObject _source, int _damage);
    private void Death(GameObject _source) { }
}
